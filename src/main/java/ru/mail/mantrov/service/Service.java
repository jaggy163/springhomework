package ru.mail.mantrov.service;

import ru.mail.mantrov.dao.Dao;
import ru.mail.mantrov.errors.NotFoundException;

import java.util.List;

public interface Service<O> {

    /*
    Returns all players from the table
     */
    default List<O> findAll() {
        return getDao().findAll();
    }
    default void create(O o) {
        getDao().create(o);
    }
    default O findBy(long id) {
        O o = getDao().findBy(id);
        if (o==null) {
            throw new NotFoundException(id);
        }
        return o;
    }
    default void update(O o) {
        getDao().update(o);
    }
    default void delete(long id) {
        getDao().delete(id);
    }
    Dao<O> getDao();
}
