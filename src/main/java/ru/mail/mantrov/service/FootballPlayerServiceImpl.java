package ru.mail.mantrov.service;

import ru.mail.mantrov.dao.FootballPlayerDao;
import ru.mail.mantrov.domain.Player;

import java.util.List;

@org.springframework.stereotype.Service
public class FootballPlayerServiceImpl implements FootballPlayerService {
    private final FootballPlayerDao dao;

    public FootballPlayerServiceImpl(FootballPlayerDao dao) {
        this.dao = dao;
    }

    @Override
    public FootballPlayerDao getDao() {
        return dao;
    }

    /*
    Returns all players from the city - 'cityName'. Realized throw the JPQL
     */
    public List<Player> findAllPlayersFromCityJPQL(String cityName) {
        return dao.findAllPlayersFromCityJPQL(cityName);
    }

    /*
    Returns all players from the city - 'Moscow'. Realized throw the NativeQuery
     */
    public List<String> findAllPlayersFromMoscowNativeQuery() {
        return dao.findAllPlayersFromMoscowNativeQuery();
    }

    /*
    Returns all players from the city - 'cityName'. Realized throw the Criteria API
     */
    public List<Player> findAllPlayersFromCityCriteria(String cityName) {
        return dao.findAllPlayersFromCityCriteria(cityName);
    }
}
