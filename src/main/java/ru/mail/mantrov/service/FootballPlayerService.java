package ru.mail.mantrov.service;

import ru.mail.mantrov.dao.FootballPlayerDao;
import ru.mail.mantrov.domain.Player;

import java.util.List;

public interface FootballPlayerService extends Service<Player> {

    /*
    Returns all players from the city - 'cityName'. Realized throw the NamedQuery.
    Requires FootballPlayerDao returning by method getDao().
     */
    default List<Player> findAllPlayersFromCityNamedQuery(String cityName) {
        FootballPlayerDao dao = (FootballPlayerDao) getDao();
        return dao.findAllPlayersFromCityNamedQuery(cityName);
    }
}
