package ru.mail.mantrov.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.mail.mantrov.domain.Player;
import ru.mail.mantrov.service.FootballPlayerService;

import java.util.List;

@Validated
@RestController
@RequestMapping("/players")
public class FootballPlayerController {
    private final FootballPlayerService service;

    FootballPlayerController(FootballPlayerService service) {
        this.service = service;
    }

    @GetMapping(value = "/list")
    public List<Player> findAllPlayers() {
        return service.findAll();
    }

    @GetMapping(value = "/id={id}")
    public ResponseEntity<Player> findBy(@PathVariable("id") int id) {
        Player player = service.findBy(id);
        return new ResponseEntity(player, HttpStatus.OK);
    }

    @GetMapping(value = "/city={city}")
    public List<Player> findAllPlayersFromCity(@PathVariable("city") String city) {
        return service.findAllPlayersFromCityNamedQuery(city);
    }

    @PostMapping
    public ResponseEntity createPlayer(@Validated(Player.class) @RequestBody Player player) {
        service.create(player);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/id={id}")
    public ResponseEntity deletePlayer(@PathVariable("id") int id) {
        service.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PutMapping
    public void updatePlayer(@RequestBody @Validated(Player.class) Player player) {
        service.update(player);
    }
}
