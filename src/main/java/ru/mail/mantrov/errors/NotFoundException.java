package ru.mail.mantrov.errors;

public class NotFoundException extends RuntimeException {
    public NotFoundException(String message) {
        super(message);
    }
    public NotFoundException(long id) {
        super("id : " + id + " not found.");
    }
}
