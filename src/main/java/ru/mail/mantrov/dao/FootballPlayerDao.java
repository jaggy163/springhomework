package ru.mail.mantrov.dao;

import ru.mail.mantrov.domain.Player;

import java.util.List;

public interface FootballPlayerDao extends Dao<Player> {
    List<Player> findAllPlayersFromCityNamedQuery(String city);
    List<Player> findAllPlayersFromCityJPQL(String city);
    List<String> findAllPlayersFromMoscowNativeQuery();
    List<Player> findAllPlayersFromCityCriteria(String city);
}
