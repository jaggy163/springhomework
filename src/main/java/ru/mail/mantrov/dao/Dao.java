package ru.mail.mantrov.dao;

import java.util.List;

public interface Dao<O> {
    List<O> findAll();
    void create(O o);
    O findBy(long id);
    void update(O o);
    void delete(long id);
}
