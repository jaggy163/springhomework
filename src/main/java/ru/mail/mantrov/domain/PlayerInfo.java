package ru.mail.mantrov.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@ToString
@Entity
@Table(name="player_info")
public class PlayerInfo {
    @Id
    private long id;

    @JsonIgnore
    @ToString.Exclude
    @OneToOne(mappedBy = "playerInfo")
    private Player player;

    @Column(name = "birth_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd")
    private LocalDate birthDate;

    @Column(name = "number")
    private int number;
}
