package ru.mail.mantrov.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@Entity
@Table(name="cities")
public class City {
    @Id
    @GeneratedValue
    private long id;

    @Column(name="name")
    private String name;

    @JsonIgnore
    @ToString.Exclude
    @OneToMany(mappedBy = "city")
    private List<Club> clubs = new ArrayList<>();
}
