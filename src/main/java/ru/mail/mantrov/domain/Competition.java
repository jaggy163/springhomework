package ru.mail.mantrov.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@ToString
@Entity
@Table(name="competitions")
public class Competition {
    @Id
    @GeneratedValue
    private long id;

    @Column(name = "competition_name")
    private String competitionName;

    @JsonIgnore
    @ToString.Exclude
    @ManyToMany(mappedBy = "competitions")
    private Set<Club> clubs = new HashSet<>();
}
