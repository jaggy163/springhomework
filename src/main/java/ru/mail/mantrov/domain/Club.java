package ru.mail.mantrov.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@ToString
@Entity
@Table(name = "clubs")
@NamedQueries({
        @NamedQuery(name = Club.FIND_ALL_CLUBS_WITH_PLAYERS, query = "SELECT c\n" +
                " FROM Club c\n" +
                " JOIN c.players p\n" +
                " GROUP BY c\n" +
                " HAVING COUNT(p) >= :minPlayerCount")
})
public class Club {
    public static final String FIND_ALL_CLUBS_WITH_PLAYERS = "findClubsNamedQuery";

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne
    @JoinColumn(name = "city_id")
    private City city;

    @JsonIgnore
    @ToString.Exclude
    @OneToMany(mappedBy = "club")
    private List<Player> players = new ArrayList<>();

    @Column(name = "title")
    private String title;

    @JsonIgnore
    @ManyToMany
    @JoinTable(
            name = "club_competition",
            joinColumns = @JoinColumn(name = "id_club", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "id_competition", referencedColumnName = "id")
    )
    private Set<Competition> competitions = new HashSet<>();

    public List<Player> getPlayers() {
        return players;
    }
}
