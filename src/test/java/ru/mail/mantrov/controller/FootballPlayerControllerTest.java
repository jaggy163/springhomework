package ru.mail.mantrov.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.mail.mantrov.domain.Player;
import ru.mail.mantrov.service.FootballPlayerService;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-config.xml")
@ActiveProfiles("local")
public class FootballPlayerControllerTest {

    private static final int ID = 0;
    private static final String CITY = "Samara";

    @Autowired
    private Player player;

    private ObjectMapper objectMapper = Jackson2ObjectMapperBuilder.json().build();

    private MockMvc mock;

    @Mock
    private FootballPlayerService service;

    @Before
    public void beforeClass() {
        MockitoAnnotations.initMocks(this);
        mock = MockMvcBuilders.standaloneSetup(new FootballPlayerController(service)).build();
    }

    @Test
    public void findAllPlayers() throws Exception {
        List<Player> players = Collections.singletonList(player);
        when(service.findAll()).thenReturn(players);
        mock.perform(get("/players/list"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(players)));
        verify(service).findAll();
    }

    @Test
    public void findBy() throws Exception {
        when(service.findBy(ID)).thenReturn(player);

        mock.perform(get("/players/id={id}", ID))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(player)));

        verify(service).findBy(ID);
    }

    @Test
    public void findAllPlayersFromCity() throws Exception {
        List<Player> players = Collections.singletonList(player);
        when(service.findAllPlayersFromCityNamedQuery(CITY)).thenReturn(players);
        mock.perform(get("/players/city={city}", CITY))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(players)));
        verify(service).findAllPlayersFromCityNamedQuery(CITY);
    }

    @Test
    public void createPlayer() throws Exception {
        doNothing().when(service).create(player);
        mock.perform(post("/players")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(player)))
                .andDo(print())
                .andExpect(status().isCreated());
        verify(service).create(player);
    }

    @Test
    public void deletePlayer() throws Exception {
        doNothing().when(service).delete(ID);
        mock.perform(delete("/players/id={id}", ID))
                .andDo(print())
                .andExpect(status().isNoContent());
        verify(service).delete(ID);
    }

    @Test
    public void updatePlayer() throws Exception {
        doNothing().when(service).update(player);
        mock.perform(put("/players")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(player)))
                .andDo(print())
                .andExpect(status().isOk());
        verify(service).update(player);
    }

}